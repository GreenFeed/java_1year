package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int i = 0,N;

        Scanner scanner = new Scanner(System.in);
        int max=0,currentNumber;

        System.out.print("Input N: ");
        N = scanner.nextInt();

        while (i<N)
        {
            System.out.print("Input number: ");
            currentNumber = scanner.nextInt();

            if(i==0)
            {
                max = currentNumber;
            }
            else
            {
                if(currentNumber>max)
                {
                    max = currentNumber;
                }
            }
            i++;
        }
        System.out.print("max: " +max);
    }
}
